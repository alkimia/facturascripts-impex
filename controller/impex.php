<?php
/*
 * This file is part of FacturaScripts
 * Copyright (C) 2017 José Martinez Barrionuevo pepemartinezb@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of opciones_servicios
 *
 * @author carlos
 */
require_model('impex.php');

class impex extends fs_controller
{

    public $allow_delete;
    public $vega_setup;
    public $version_api = '1.0';

    public function __construct()
    {
         parent::__construct(__CLASS__, 'Importar/Exportar', 'admin');
    }

    protected function private_core()
    {
        /// ¿El usuario tiene permiso para eliminar en esta página?
        $this->allow_delete = $this->user->allow_delete_on(__CLASS__);

        $this->check_menu();
        $this->share_extensions();

        $impex = new impexModel();

        if ($_POST) {
            
            $res_impex = $impex->import_entity();
        }        

        $this->import_export = $impex->get_process_list();

    }

    private function share_extensions()
    {
/*        $fsext = new fs_extension();
        $fsext->name = 'opciones_servicios';
        $fsext->from = __CLASS__;
        $fsext->to = 'ventas_servicios';
        $fsext->type = 'button';
        $fsext->text = '<span class="glyphicon glyphicon-wrench" aria-hidden="true">'
            . '</span><span class="hidden-xs">&nbsp; Opciones</span>';
        $fsext->save();*/
    }

    private function check_menu()
    {
        if (!$this->page->get('ventas_servicios')) {
            if (file_exists(__DIR__)) {
                /// activamos las páginas del plugin
                foreach (scandir(__DIR__) as $f) {
                    if ($f != '.' AND $f != '..' AND is_string($f) AND strlen($f) > 4 AND ! is_dir($f) AND $f != __CLASS__ . '.php') {
                        $page_name = substr($f, 0, -4);

                        require_once __DIR__ . '/' . $f;
                        $new_fsc = new $page_name();

                        if (!$new_fsc->page->save()) {
                            $this->new_error_msg("Imposible guardar la página " . $page_name);
                        }

                        unset($new_fsc);
                    }
                }
            } else {
                $this->new_error_msg('No se encuentra el directorio ' . __DIR__);
            }

            $this->load_menu(TRUE);
        }
    }

}
