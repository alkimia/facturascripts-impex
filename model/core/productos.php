<?php
require_once 'plugins/facturacion_base/model/core/articulo.php';

class impex_product {

  public function __construct($data = FALSE) {
    global $db;

    $sql = "SHOW TABLES LIKE 'impex_log'";

    $data = $db->select($sql);
    if (!$data) {
      $sql = 'CREATE TABLE `impex_log` (
        `request_id` tinyint(4) DEFAULT NULL,
        `message` varchar(255) DEFAULT NULL,
        `data` text,
        KEY `impex_request_id` (`request_id`),
        CONSTRAINT `impex_log_ibfk_1` FOREIGN KEY (`request_id`) REFERENCES `impex_request` (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;';
      $db->exec($sql);              
    }      

  }     

/*  public function check_product_exists($params) {
      extract($params);

      global $db;

      $sql = "SELECT * FROM articulos WHERE $field='$value'";    

      $data = $db->select($sql);
      if ($data) {
        return true;
      } else {
        return false;
      }


   }	*/

   public function exists() {
	      return parent::exists();
   }

    public function save()
    {
    }

/*    public function insert($row) {
    	global $db;

    	$fields = array();
    	$values = array();

    	foreach($row as $key => $value) {
    		$fields[] = $key;
    		$values[] = "'" . $value . "'";
    	}

    	$values = str_replace("'NULL'", "NULL", $values);

        $sql = "INSERT INTO articulos (" . implode(',', $fields) . ") VALUES (" . implode("," , $values) . ");";
    	$db->exec($sql); 


                    $regularizacion = new regularizacion_stock();
                    $regularizacion->idstock = $stock->idstock;
                    $regularizacion->cantidadini = floatval($_POST['cantidadini']);
                    $regularizacion->cantidadfin = floatval($_POST['cantidad']);
                    $regularizacion->codalmacendest = $_POST['almacen'];
                    $regularizacion->motivo = $_POST['motivo'];
                    $regularizacion->nick = $this->user->nick;    	   	
    }*/

    public function delete()
    {
    }    

    public function log_register($params_log_register) {
      global $db;
      extract($params_log_register);
      $sql = "INSERT INTO impex_log (request_id, message, data) VALUES ($request_id, '$message', '$data')";
      $db->exec($sql);       
    }

    public function set_stock($params_set_stock) {      
      global $db;
      extract($params_set_stock); 

      if ($options == "overwrite_stock") {
        $sql = "UPDATE articulos SET stockfis=" . $stock . ", nostock=1, controlstock=0 WHERE referencia = '" . $referencia . "';";
        $db->exec($sql);   

      } else if ($options == "update_stock") {

        $sql = "UPDATE articulos SET stockfis=" . $stock . ", nostock=0, controlstock=1 WHERE referencia = '" . $referencia . "';";
        $db->exec($sql);   


        $fecha = date("Y-m-d");      
        $hora = date("H:i:s");  


        $sql="SET FOREIGN_KEY_CHECKS=0";
        $db->exec($sql);         


        $sql = "SELECT * FROM stocks WHERE referencia = '$referencia'";
        $res = $db->select($sql);
          if (count($res) == 0) {
              $sql = "INSERT INTO stocks (referencia, codalmacen) VALUES ('$referencia','ALG')";
              $db->exec($sql);      

              $idstock = $db->lastval();            
          } else {
              $idstock = $res[0]['idstock'];
          }

        $sql = "SELECT * FROM lineasregstocks WHERE idstock='$idstock' ORDER BY id DESC LIMIT 1";
        $res = $db->select($sql);
          if (count($res) == 0) {
            $cantidadini = 0;
          } else {
            $cantidadini = $res[0]['cantidadfin'];

          }

        $cantidadfin = $stock;

        if ($idstock == 0 || $idstock =='') {
            echo $sql;
          die('flag!');
        }
        

        $sql = "INSERT INTO lineasregstocks
        (cantidadfin,
        cantidadini,
        codalmacendest,
        fecha,
        hora,
        idstock,
        motivo,
        nick)
        VALUES
        ($cantidadfin,
        $cantidadini,
        'ALG',
        '$fecha',
        '$hora',
        $idstock,
        'Importacion XML',
        'admin')";

        $db->exec($sql);

        $sql = "UPDATE stocks SET disponible=$stock, cantidad=$stock WHERE referencia='$referencia'";
        $db->exec($sql);

        $sql="SET FOREIGN_KEY_CHECKS=1";
        $db->exec($sql);         

      }


    }

   public function product_prepare_row($data) {

    $row = array (
      "referencia" => $data->code,      
      "tipo" => NULL,  
      "codfamilia" => NULL,
      "codfabricante" => NULL,      
      "descripcion" => $data->name,
      "pvp" => $data->price,          
      "factualizado" => date('Y-m-d H:i:s'),
      "costemedio" => '0',
      "preciocoste" => $data->cost,
      "codimpuesto" => 'IVA21',
      "stockfis" => $data->stock,
      "stockmin" => '0',
      "stockmax" => '0',
      "controlstock" => '1',
      "bloqueado" => '0',
      "secompra" => '0',      
      "sevende" => '1',
      "publico" => '0',      
      "equivalencia" => $data->equivalent,
      "partnumber" => '',      
      "codbarras" => $data->ean13,
      "observaciones" => $data->obs."<br>Localizacion: " . $data->local . "<br>Unidades por caja: " . $data->unisperbox,      
      "idsubcuentairpfcom" => '',
      "codsubcuentairpfcom" => NULL,
      "codsubcuentacom" => NULL,
      "idsubcuentacom" => '',
      "trazabilidad" => '0',
      "imagen" => '',
      "nostock" => false
    );    

    return $row;
   }

   public function update_request_status($params_update_request) {
    global $db;
    //extract($params_update_request);
    $request_id = $params_update_request['request_id'];

    foreach($params_update_request as $key => $value) {
      if ($key != 'request_id') {
        $sql = "UPDATE impex_request SET $key='$value' WHERE id='$request_id'";
        echo $sql;
        $db->exec($sql);    
      }
    }

    
   }

   public function parse_xml($params_parse_xml) {
    extract($params_parse_xml);

    $processed = 0;
    $errors = 0;
    $start_xml = date("Y-m-d H:i:s");
    $entity = 'productos';

    $params_update_request = array (
      "start_xml" => $start_xml,
      "entity" => 'productos',
      "request_id" => $request_id,
      "status" => "en curso"
    );

    $this->update_request_status($params_update_request);    


    foreach($xml->items->product as $product) {

      // mapping and exchange fields before insert/update          
      $row = $this->product_prepare_row($product);

      $articulo = new articulo($row);
      $articulo->save();

      if (!$articulo->save()) {
        $params_log_register = array ("request_id" => $request_id, "message" => "Error al registrar producto", "data" => json_encode($row));
        $this->log_register($params_log_register);  
        
        ++ $errors;
      } else {

        if ($options != 'ignore_stock') {

          // actualizar stock
          $params_set_stock = array (
              "stock" => (string) $product->stock,
              "referencia" => (int) $product->code,
              "options" => $options
          );

          $this->set_stock($params_set_stock);

        }
      }

      ++$processed;
    }


    $params_update_request = array (
      "start_xml" => $start_xml,
      "stop_xml" => date("Y-m-d H:i:s"),
      "processed" => $processed,
      "errors" => $errors,
      "entity" => 'productos',
      "request_id" => $request_id,
      "status" => "finalizado"
    );

    $this->update_request_status($params_update_request);
   }	

}