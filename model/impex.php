<?php
/* https://stackoverflow.com/questions/7604436/xmlparseentityref-no-name-warnings-while-loading-xml-into-a-php-file */
require 'core/productos.php';

class impexModel extends fs_model {
    private $id;
   private $tipo;
   private $nombrearchivo;
   private $entidad;
   private $inicio;
   private $fin;
   private $procesados;
   private $errores;
   private $estado;
   private $opciones;

   public function get_process_list() {
      $sql = "SELECT * FROM impex_request";
      return $this->db->select($sql);
/*      if ($data) {
          foreach ($data as $row) {
              $rows[] = $row;
          }
      }

      return $rows;*/
      //return $data;
   }

	public function import_entity() {
        $key = "upload_xml";                
        $hay_fichero = $_FILES[$key]['name'];    
    
        if ($hay_fichero<>"") {

            if ($_FILES[key]["size"]<850000) { // 8m
            
                $hay_fichero=str_replace(" ","_",$hay_fichero);
    
                $nombre_fichero=rand(1,500)."-".$hay_fichero;
                //if ($_FILES["fichero"]["type"]=="image/jpeg"||$_FILES["fichero"]["type"]=="image/pjpeg"||$_FILES["fichero"]["type"]=="image/png") { 
                // Si hay archivo subelo
                if (!(is_uploaded_file($_FILES[$key]["tmp_name"]))) { die('EL fichero no ha subido!!'); }
                if (!(move_uploaded_file($_FILES[$key]["tmp_name"], 'tmp/xml-files/'.$nombre_fichero))) { die('Error al mover archivo!!'); }

                $options = $_POST['options'];

                $sql = "INSERT INTO impex_request (filename, status, options) VALUES ('$nombre_fichero','pending','$options')";                
    
                $result = $this->db->exec($sql);

                return "uploaded_ok";
                    
            } else {

            	return "too_big_file";

            }
        }	
	}

    public function __construct($data = FALSE) {
      parent::__construct('impex_request');

      //$this->add_keyfield('id');

      if ($data) {
         $this->load_from_data($data);
      } else {
         $this->clear();
      }
   }   

    public function exists() {
      return parent::exists();
   }

  public function install() {
      return '';
   }   

   protected function test() {
      /*
        PUT HERE MODEL DATA VALIDATIONS
        EXAMPLE:
        if($this->field_Numeric == 0) {
        $this->new_error_msg('Must be inform a code value');
        return FALSE;
        }
        return TRUE;
       */
      return parent::test();
   } 

    public function save()
    {
    }

    public function delete()
    {
    }     

/*  protected function update() {
      $sql = 'UPDATE impex_request SET '
              . '  field1 = value1'
              . ', fieldN = valueN'
              . ' WHERE field_key1 = key_value1;';

      return $this->db->exec($sql);
   }

   protected function insert() {
      $sql = 'INSERT INTO impex_request '
              . '(id,tipo,nombrearchivo,entidad,inicio,fin,procesados,errores,estado,opciones)'
              . ' VALUES '
              . '(...);';

      return $this->db->exec($sql);
   }   */

   public function clear() {
      $this->id = '';
      $this->tipo = '';
      $this->nombrearchivo = '';
      $this->entidad = '';
      $this->inicio = '';
      $this->fin = '';
      $this->procesados = '';
      $this->errores = '';
      $this->estado = '';
      $this->opciones = '';
   } 

   public function load_from_data($data) {
      $this->id = $data['id'];
      $this->tipo = $data['tipo'];
      $this->nombrearchivo = $data['nombrearchivo'];
      $this->entidad = $data['entidad'];
      $this->inicio = $data['inicio'];
      $this->fin = $data['fin'];
      $this->procesados = $data['procesados'];
      $this->errores = $data['errores'];
      $this->estado = $data['estado'];
      $this->opciones = $data['opciones'];
   } 

    public function run_cron()
    {
        $sql = "SELECT * FROM impex_request WHERE status='pending'";    
        $data = $this->db->select($sql);
        
        $xml_malformed = false;

        $valid_entities = ["products"];

        if ($data) {
            foreach ($data as $d) {            
                
                $xml = simplexml_load_file('tmp/xml-files/' . $d['filename']);      

                if ($xml == false) {
                  $xml_malformed = true;
                } else {
                  if (!in_array($xml->entity, $valid_entities)) {
                    $xml_malformed = true;
                  }
                }

                if ($xml_malformed == false) {
                
                  switch($xml->entity) {
                    case 'products':
                      $impex_product = new impex_product();

                      $params_parse_xml = array (
                        "xml" => $xml,
                        "request_id" => $d['id'],
                        "options" => $d['options']
                      );

                      $res_import = $impex_product->parse_xml($params_parse_xml);
                      break;
                  }

                } else {
                    $params_update_request = array (
                      "stop_xml" => date("Y-m-d H:i:s"),
                      "processed" => '0',
                      "errors" => '0',
                      "entity" => 'productos',
                      "request_id" => $d['id'],
                      "status" => "abortado: error en xml"
                    );

                    impex_product::update_request_status($params_update_request);                  
                }

          }

      }
  }
}